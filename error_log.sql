USE [admin]
GO

/****** Object:  Table [dbo].[error_log]    Script Date: 11/11/2020 12:30:54 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[error_log](
	[entry_time] [datetime] NULL,
	[number] [int] NULL,
	[severity] [int] NULL,
	[state] [int] NULL,
	[LINE] [int] NULL,
	[procedure] [nvarchar](128) NULL,
	[message] [nvarchar](4000) NULL
) ON [PRIMARY]
GO


