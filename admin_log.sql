USE [admin]
GO

/****** Object:  Table [dbo].[admin_log]    Script Date: 11/11/2020 12:30:16 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[admin_log](
	[entry_time] [datetime] NOT NULL,
	[log_type] [varchar](32) NOT NULL,
	[db] [varchar](128) NULL,
	[cmd] [varchar](max) NULL,
	[msg] [varchar](max) NULL,
	[status] [varchar](16) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


