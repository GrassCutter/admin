USE [admin]
GO
/****** Object:  StoredProcedure [dbo].[p_backup]    Script Date: 11/10/2020 10:06:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[p_backup]
@backup_type varchar(8),
@db varchar(256) = NULL
AS

declare @DatabaseName varchar(128),
		@sql varchar(max),
		@parameter varchar(256),
		@location varchar(256),
		@startdate datetime,
		@stamp varchar(32)


If @db is not null
	Begin
		
		Select @location = [value] from [admin].[dbo].[config] where [type] = 'backup' and [name] = @backup_type +'_location'
		Select @parameter = [value] from [admin].[dbo].[config] where [type] = 'backup' and [name] = @backup_type + '_parameter'
		
		Set @stamp = replace(replace(replace(replace(convert(varchar, getdate(), 121), '-', ''), '.',''), ':', ''), ' ', '_')

		Set @sql = 
			Case @backup_type 
				When 'Full' Then 'Backup Database ['+@db+'] TO DISK = N'''+@location+'\'+@db+'_'+@stamp+'.bak'' WITH '+@parameter+''
				When 'Diff' Then 'Backup Database ['+@db+'] TO DISK = N'''+@location+'\'+@db+'_'+@stamp+'.diff'' WITH Differential, '+@parameter+''
				When 'Tlog' Then 'Backup Log ['+@db+'] TO DISK = N'''+@location+'\'+@db+'_'+@stamp+'.trn'' WITH '+@parameter+''
			END
		
		BEGIN TRY  
			--DBs in SIMPLE can't have tlog backup
			iF (select recovery_model_desc from sys.databases Where [name] = @db) = 'SIMPLE' AND @backup_type = 'Tlog'
				Begin
					Insert Into admin_log (entry_time, log_type, db, [status], msg)
					Values (@startdate, @backup_type + '_backup', @db, 'SKIPPED', 'DB ['+@db+'] is currently in simple recovery')

					Print 'DB ['+@db+'] is currently in simple recovery'

				End
			Else
				Begin

					Set @startdate = getdate()
			
					Insert Into admin_log (entry_time, log_type, db, cmd,[status])
					Values (@startdate, @backup_type + '_backup', @db, @sql, 'started')

					Execute(@sql)

					Update admin_log
					set msg = 'Backup of ['+@db+'] completed successfully at '+convert(varchar(32), getdate())+' Duration: ' +convert(varchar(128), DateDiff(s, @startdate, getdate()))+' seconds.'
					Where entry_time = @startdate

					Update admin_log
					set status = 'success'
					Where entry_time = @startdate


				END

		END TRY  

		BEGIN CATCH  
			
			Update admin_log
			set status = 'FAILURE',
			msg = ISNULL(convert(varchar(128), ERROR_NUMBER()),'no error number') + ' '+ ISNULL(convert(varchar(256), ERROR_MESSAGE()),'no error msg')
			Where entry_time = @startdate

			Insert Into error_log
									Values(
										getdate(),
										ERROR_NUMBER(),
										ERROR_SEVERITY(),
										Error_State(),
										ERROR_LINE(),
										ERROR_PROCEDURE(),
										ERROR_MESSAGE()
									)

		END CATCH;   

	End
Else --if @db isn't defined
	Begin
			declare db_cursor cursor for
				Select [Name] From sys.databases
				Where state = 0 --online
				and [name] not in ('model','tempdb')
				open db_cursor

			    fetch next from db_cursor into @db
   
			    while @@fetch_status = 0
					begin
						
								Select @location = [value] from [admin].[dbo].[config] where [type] = 'backup' and [name] = @backup_type +'_location'
								Select @parameter = [value] from [admin].[dbo].[config] where [type] = 'backup' and [name] = @backup_type + '_parameter'
		
								Set @stamp = replace(replace(replace(replace(convert(varchar, getdate(), 121), '-', ''), '.',''), ':', ''), ' ', '_')

								Set @sql = 
									Case @backup_type 
										When 'Full' Then 'Backup Database ['+@db+'] TO DISK = N'''+@location+'\'+@db+'_'+@stamp+'.bak'' WITH '+@parameter+''
										When 'Diff' Then 'Backup Database ['+@db+'] TO DISK = N'''+@location+'\'+@db+'_'+@stamp+'.diff'' WITH Differential, '+@parameter+''
										When 'Tlog' Then 'Backup Log ['+@db+'] TO DISK = N'''+@location+'\'+@db+'_'+@stamp+'.trn'' WITH '+@parameter+''
									END
		
								BEGIN TRY  
									
									Set @startdate = getdate()
									
									--DBs in SIMPLE can't have tlog backup
									iF (select recovery_model_desc from sys.databases Where [name] = @db) = 'SIMPLE' AND @backup_type = 'Tlog'
										Begin
											Insert Into admin_log (entry_time, log_type, db, [status], msg)
											Values (@startdate, @backup_type + '_backup', @db, 'SKIPPED', 'DB ['+@db+'] is currently in simple recovery')

											Print 'DB ['+@db+'] is currently in simple recovery'

										End
									Else
										BEGIN
											Insert Into admin_log (entry_time, log_type, db, cmd,[status])
											Values (@startdate, @backup_type + '_backup', @db, @sql, 'started')

											Execute(@sql)

											Update admin_log
											set msg = 'Backup of ['+@db+'] completed successfully at '+convert(varchar(32), getdate())+' Duration: ' +convert(varchar(128), DateDiff(s, @startdate, getdate()))+' seconds.'
											Where entry_time = @startdate

											Update admin_log
											set status = 'success'
											Where entry_time = @startdate
										END

								END TRY  

										BEGIN CATCH  
			
											Update admin_log
											set status = 'FAILURE',
											msg = ISNULL(convert(varchar(128), ERROR_NUMBER()),'no error number') + ' '+ ISNULL(convert(varchar(256), ERROR_MESSAGE()),'no error msg')
											Where entry_time = @startdate
											

										Insert Into error_log
											Values (
												getdate(),
												ERROR_NUMBER(),
												ERROR_SEVERITY(),
												Error_State(),
												ERROR_LINE(),
												ERROR_PROCEDURE(),
												ERROR_MESSAGE()
											)
										END CATCH;  

					 fetch next from db_cursor into @db
				   end
				
				close db_cursor
				deallocate db_cursor
End





