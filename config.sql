USE [admin]
GO

/****** Object:  Table [dbo].[config]    Script Date: 11/11/2020 12:31:23 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[config](
	[type] [varchar](32) NULL,
	[name] [varchar](128) NULL,
	[value] [varchar](1048) NULL
) ON [PRIMARY]
GO


